import numpy as np
import pandas as pd
from PIL import Image
from random import shuffle
import os

def load_data(df_train,PATH,IMG_SIZE,convert=False,flip = False):
  train_dir = os.path.join(PATH, 'train')

  data = []
  
  nb_ax = 1 if convert else 3

  for img in os.listdir(train_dir):
    label = df_train.loc[img][1]
    path = os.path.join(train_dir, img)
    img = Image.open(path)
    if convert:
      img = img.convert('L')
    img = img.resize((IMG_SIZE, IMG_SIZE), Image.ANTIALIAS)
    data.append([np.array(img), label])

    if flip :
      # Basic Data Augmentation - Horizontal Flipping
      flip_img = Image.open(path)
      if convert:
        flip_img = flip_img.convert('L')
      flip_img = flip_img.resize((IMG_SIZE, IMG_SIZE), Image.ANTIALIAS)
      flip_img = np.array(flip_img)
      flip_img = np.fliplr(flip_img)
      data.append([flip_img, label])

  shuffle(data)

  dataX = np.array([i[0] for i in data]).reshape(-1, IMG_SIZE, IMG_SIZE,nb_ax)
  dataY = np.array([i[1] for i in data])


  def shuffle_home(matrix, target, test_proportion):
    ratio = int(matrix.shape[0]/test_proportion)
    X_train = matrix[:ratio]
    X_test =  matrix[ratio:]
    Y_train = target[:ratio]
    Y_test =  target[ratio:]
    return X_train, X_test, Y_train, Y_test

  return shuffle_home(dataX, dataY, 1.5)